<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportPD\app\http\controllers\ImportPdController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/pd/',
    [ImportPdController::class, 'index'])
    ->name('imports.pds.index');
